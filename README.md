# Introduction

This Docker images allows you to easily test drive the new Magento 2 (currently still in beta) without going through the lengthy installation procedure (it includes the 'sample data').

# Running the Magento 2 Demo image

To run the image only 2 very simple steps are required:

* Edit your hosts file (Magento 2 also relies on a base URL, so you will need to add an entry to your local hosts file):
```
<IP of your Docker host>	magento2-demo.local
```

* Run the container:
```
docker run -d -p 80:80 phpro/magento2-demo
```

# Visiting the Magento 2 Demo webshop/backend

- You can visit the Magento 2 Demo webshop using the following URL:
    - [http://magento2-demo.local/](http://magento2-demo.local/)
- You can visit the Magento 2 Demo backend using the following URL:
    - [http://magento2-demo.local/admin/](http://magento2-demo.local/admin/) (admin/password123)

# Current versions
- 1.0.0-beta